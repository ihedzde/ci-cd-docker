using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DTO.Selects;
using Common.DTO.Task;

namespace BLL.Domain.Services
{
    public interface IAggregateServices
    {
        Task<UserTaskCountUnfinishedTaskCountLongestTask> ComplexUserEntity(int userId);
        Task<IList<IdAndName>> FinishedIn2021(int userId);
        Task<IList<TaskDTO>> GetLessThan45(int userId);
        Task<Dictionary<string, int>> GetTasksEntitiesById(int userId);
        Task<IList<ProjectTaskUserCountMix>> OddlyProjectTaskUserCountMix();
        Task<IList<IGrouping<string, IdTeamNameMembers>>> TeamsOlder10Sorted();
        Task<IList<IdUserAndTasks>> UsersAlphabeticByTasksLength();
    }
}