using System.Collections.Generic;
using System.Text;
using Common.DTO.User;

namespace Common.DTO.Selects
{
    public struct IdTeamNameMembers
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IList<UserDTO> Members{ get; set; }
        
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach(var member in Members){
                builder.AppendLine(member.ToString());
            }
            
            return $"Id: {Id} Name: {TeamName} Members: {builder.ToString()}";
        }
    }
}