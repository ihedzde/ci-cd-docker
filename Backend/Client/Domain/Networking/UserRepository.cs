using Newtonsoft.Json;
using Client.Domain.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.Domain.Interfaces;
using Client.Domain.Models;
using Common.DTO.User;
using System.Text;

namespace Client.Domain.Networking
{
    public class UserRepository : IDisposable, IRepository<UserModel, CreateUserDTO>
    {
        private readonly HttpClient _client;
        private readonly string _route = Routes.User.ToString();

        public UserRepository()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.FetchUrlBase);
        }

        public void Dispose()
        {
            _client.Dispose();
        }


        public async Task<IList<UserModel>> Read()
        {
            var response = await _client.GetAsync(_route);
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<UserModel>>(json);
        }

        public async Task<UserModel> Create(CreateUserDTO data)
        {
            var response = await _client.PostAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            return JsonConvert.DeserializeObject<UserModel>(await response.Content.ReadAsStringAsync());
        }

        public async Task Update(UserModel data)
        {
            await _client.PutAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
        }

        public async Task Delete(int id)
        {
            await _client.DeleteAsync(_route + $"?id={id}");
        }
    }
}
