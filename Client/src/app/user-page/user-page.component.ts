import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  navigationSubscription: Subscription;
  constructor(private route: ActivatedRoute, private userService: UserService) {
    this.route.params.pipe(
      switchMap(params => {
        const reloadToken = params['reloadToken'];
        if (reloadToken !== 'none')
          this.users$ = this.userService.getUsers$();
        return this.users$;
      }),
    );
  }
  public users$: Observable<User[]>;


  ngOnInit(): void {
    this.users$ = this.userService.getUsers$();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
}
