import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FormComponent } from 'src/app/guard/form.guard';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-user-dialog',
  templateUrl: './update-user-dialog.component.html',
  styleUrls: ['./update-user-dialog.component.scss']
})
export class UpdateUserDialogComponent implements OnInit, FormComponent  {

  constructor(public userService: UserService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  user$: Observable<User>;
  ngOnInit(): void {
   this.user$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params['id'];
        return this.userService.getById$(id);
      }),
    );
  }
  onSubmit(data:User) {
    this.submitedOrGood = true;
    this.userService.updateUser$(data).subscribe();
    this.router.navigate(['../../../updated'], { relativeTo: this.route }).then(()=>window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../../none'], { relativeTo: this.route });
  }
}
