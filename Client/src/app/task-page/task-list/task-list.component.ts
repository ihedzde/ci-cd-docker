import { Task } from './../../models/task';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskService } from 'src/app/services/task-service.service';
import { MatDialog } from '@angular/material/dialog';
import { TaskDialogComponent } from '../task-dialog/task-dialog.component';
@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  @Input('tasks$')
  public tasks$: Observable<Task[]>;
  constructor() {
  }
  ngOnInit(): void {

  }
 
}
