import { UpdateTeamDialogComponent } from './team-page/update-team-dialog/update-team-dialog.component';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectPageComponent } from './project-page/projectpage.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjectListComponent } from './project-page/project-list/project-list.component';
import { TaskPageComponent } from './task-page/task-page.component';
import { TaskListComponent } from './task-page/task-list/task-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { TaskStatePipe } from './pipes/task-state.pipe';
import { TaskFinishedDirective } from './directives/task-finished.directive';
import { DateUAPipe } from './pipes/date-ua.pipe';
import { DatePipe, registerLocaleData } from '@angular/common';
import localeUa from '@angular/common/locales/uk';
import { TaskDialogComponent } from './task-page/task-dialog/task-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ProjectDialogComponent } from './project-page/project-dialog/project-dialog.component';
import { MatNativeDateModule } from '@angular/material/core';
import { TeamPageComponent } from './team-page/team-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { TeamListComponent } from './team-page/team-list/team-list.component';
import { UserPipePipe } from './pipes/user-pipe.pipe';
import { UserListComponent } from './user-page/user-list/user-list.component';
import { UnderfinePipe } from './pipes/underfine.pipe';
import { CreateTeamDialogComponent } from './team-page/create-team-dialog/create-team-dialog.component';
import { UpdateUserDialogComponent } from './user-page/update-user-dialog/update-user-dialog.component';
import { CreateUserDialogComponent } from './user-page/create-user-dialog/create-user-dialog.component';
import {MatCardModule} from '@angular/material/card';
import { CreateTaskDialogComponent } from './task-page/create-task-dialog/create-task-dialog.component';
import { UpdateTaskDialogComponent } from './task-page/update-task-dialog/update-task-dialog.component';
import { UpdateProjectDialogComponent } from './project-page/update-project-dialog/update-project-dialog.component';
import { CreateProjectDialogComponent } from './project-page/create-project-dialog/create-project-dialog.component';
import { ProjectDeadlineDirective } from './directives/project-deadline.directive';
registerLocaleData(localeUa, 'ua');
@NgModule({
  declarations: [
    AppComponent,
    ProjectPageComponent,
    ProjectListComponent,
    TaskPageComponent,
    TaskListComponent,
    TaskStatePipe,
    TaskFinishedDirective,
    DateUAPipe,
    TaskDialogComponent,
    ProjectDialogComponent,
    TeamPageComponent,
    UpdateTeamDialogComponent,
    UpdateUserDialogComponent,
    TeamListComponent,
    UserPipePipe,
    UserPageComponent,
    UserListComponent,
    UnderfinePipe,
    CreateTeamDialogComponent,
    UpdateUserDialogComponent,
    CreateUserDialogComponent,
    CreateTaskDialogComponent,
    UpdateTaskDialogComponent,
    UpdateProjectDialogComponent,
    CreateProjectDialogComponent,
    ProjectDeadlineDirective,
  ],
  imports: [
    MatNativeDateModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSelectModule,
    MatDatepickerModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule
  ],
  providers: [DatePipe,
    { provide: LOCALE_ID, useValue: 'ua' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
